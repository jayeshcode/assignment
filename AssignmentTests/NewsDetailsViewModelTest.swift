//
//  NewsDetailsViewModelTest.swift
//  AssignmentTests
//
//  Created by Jay  on 2020-10-01.
//

import Foundation


import XCTest
@testable import Assignment

class NewsDetailsViewModelTest: XCTestCase {
    
    private var viewModel: NewsDetailsViewModel!
    private var newsViewModel = NewsViewModel()
    
    override func setUpWithError() throws {
        
        
        if let data = JsonReader.load(jsonFileName: "SwiftNews") {
            let decoder = JSONDecoder()
            
            do {
                let model = try decoder.decode(SwiftNews.self, from: data)
                newsViewModel.swiftNews = model.self
            }
        }
    }
    
    
    func testFor_DetailsTitle() {
        
        newsViewModel.createDisplayList()
        let list = newsViewModel.displayList
        
        
        // if user taps first cell from tableview
        guard let cellVM = list.first else {
            XCTFail("test preconditin fails")
            return
        }
        viewModel = NewsDetailsViewModel(smallThumbnail: cellVM.smallThumbnail , title: cellVM.title  , cellType: cellVM.type )
        
        
        // Details title shoud be present as title for news cell
        XCTAssertEqual(viewModel.cellType, .newsCell)
        XCTAssertEqual(viewModel.detailsTitle, "What’s everyone working on this month? (September 2020)")
        
        // if user taps second cell  from tableview
        let cellVMSecond = list[1]
        viewModel = NewsDetailsViewModel(smallThumbnail: cellVMSecond.smallThumbnail, title: cellVMSecond.title , cellType: cellVMSecond.type)
        
        // Details title shoud be empty for image type cell
        
        XCTAssertEqual(viewModel.detailsTitle, "")
        XCTAssertEqual(list[1].type, .imageNewsCell)
    }
    
    // Integration test for fetch image
    func testFor_FetchImage() {
        
        newsViewModel.createDisplayList()
        let list = newsViewModel.displayList
        
        
        guard let newsCellModel = list.first(where: { newsViewModel.isThumbnailPresent(childData: $0.childData)} ) else {
            XCTFail("test preconditin fails")
            return
        }
        
        viewModel = NewsDetailsViewModel(smallThumbnail: newsCellModel.smallThumbnail, title: newsCellModel.title , cellType: newsCellModel.type)
        
        let expectation = XCTestExpectation(description: "fetchImage")
        
        XCTAssertEqual(viewModel.smallThumbnail, "https://b.thumbs.redditmedia.com/YusIlT867_jmRpyfO58G-LoS4A2a1O6PgyZqOodemSQ.jpg")
        ExpiringCache.newsImages(smallThumbnail: viewModel.smallThumbnail, completion: { image in
            expectation.fulfill()
        })
        
        wait(for: [expectation], timeout: 10.0)
    }
    
    
    
    
    
    func testFor_DetailsImage() {
        newsViewModel.createDisplayList()
        let list = newsViewModel.displayList
        
        
        // if user taps first cell from tableview
        guard let cellVM = list.first else {
            XCTFail("test preconditin fails")
            return
        }
        
        viewModel = NewsDetailsViewModel(smallThumbnail: cellVM.smallThumbnail , title: cellVM.title  , cellType: cellVM.type )
        
        
        // image should be empty
        XCTAssertEqual(viewModel.smallThumbnail, "")
        
        // if user taps second cell  from tableview
        let cellVMSecond = list[1]
        viewModel = NewsDetailsViewModel(smallThumbnail: cellVMSecond.smallThumbnail, title: cellVMSecond.title , cellType: cellVMSecond.type)
        
        // smallThumbnail should be present
        XCTAssertEqual(viewModel.smallThumbnail, "https://b.thumbs.redditmedia.com/YusIlT867_jmRpyfO58G-LoS4A2a1O6PgyZqOodemSQ.jpg")
        
    }
}
