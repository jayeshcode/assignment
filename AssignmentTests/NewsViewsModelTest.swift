//
//  NewsViewsModelTest.swift
//  AssignmentTests
//
//  Created by Jay  on 2020-09-30.
//

import Foundation


import XCTest
@testable import Assignment

class NewsViewsModelTest: XCTestCase {
    
    private var viewModel: NewsViewModel!
    
    override func setUpWithError() throws {
        viewModel = NewsViewModel()
        
        if let data = JsonReader.load(jsonFileName: "SwiftNews") {
            let decoder = JSONDecoder()
            
            do {
                let model = try decoder.decode(SwiftNews.self, from: data)
                viewModel.swiftNews = model.self
            }
        }
    }
    
    
    // Integration test for fetch news
    func test_fetchSwiftNews() {
        let expectation = XCTestExpectation(description: "fetchSwiftNews")
        
        NewsService.fetchSwiftNews() { result in
            switch result {
            case .success(let newsModel):
                XCTAssertNotNil(newsModel, "No news model")
            case .failure(_ ):
                break
            }
            
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 10.0)
    }

    func testJSONDataModel() {
        // Test precondition
        guard let data = JsonReader.load(jsonFileName: "SwiftNews") else {
            XCTFail()
            return
        }
            let decoder = JSONDecoder()

            do {
                let model = try decoder.decode(SwiftNews.self, from: data)
                XCTAssertNotNil(model)
                XCTAssertNotNil(model.data)
                XCTAssertNotNil(model.data.children)
            } catch {
                XCTFail()
            }

    }
    
    func testFor_Childrens() {
        XCTAssertNotNil(viewModel.childrens)
        XCTAssertEqual(viewModel.childrens.first?.data.title, "What’s everyone working on this month? (September 2020)" )
        XCTAssertEqual(viewModel.childrens.count, 26)
    }
    
    
    func testFor_isThumbnailPresent() {
        
        let thumbnailPresent = viewModel.childrens.map {
            viewModel.isThumbnailPresent(childData: $0.data)
         
        }
        
        let expectation = [false, true,false,false, false, false, false, false, false, true, false,false, false, false, true, false, false, true, false, true, false, false, false, false, false, false]
        
        XCTAssertEqual(thumbnailPresent, expectation)
    }
    
    
    func testFor_createDisplayList() {
       viewModel.createDisplayList()
        let list = viewModel.displayList
        
        XCTAssertEqual(viewModel.displayList.count, 26)
        XCTAssertEqual(list.first?.title, "What’s everyone working on this month? (September 2020)")
        XCTAssertEqual(list.first?.type, .newsCell)
        
        XCTAssertEqual(list[1].title, "Buildwatch for Xcode")
        XCTAssertEqual(list[1].type, .imageNewsCell)
        XCTAssertEqual(list[1].childData.thumbnailWidth, 140)
        XCTAssertEqual(list[1].childData.thumbnailHeight , 140)
        XCTAssertEqual(list[1].childData.thumbnail , "https://b.thumbs.redditmedia.com/YusIlT867_jmRpyfO58G-LoS4A2a1O6PgyZqOodemSQ.jpg")
    }
    
    
    func testFor_LoblawsColor() {
        XCTAssertEqual(viewModel.loblawsColor, #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1))
        
    }
}
