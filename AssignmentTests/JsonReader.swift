//
//  JsonReader.swift
//  AssignmentTests
//
//  Created by Jay  on 2020-09-30.
//

import Foundation

// JsonReader
///
/// A helper class to load json .
final class JsonReader {
    
    /// Returns a dictionary from JSON file
    ///
    /// - Parameters:
    ///   - jsonFileName: Name of json file
    static func load(jsonFileName fileName: String) -> Data? {
        do {
            let currentBundle = Bundle(for: self)
            
            guard let file = currentBundle.url(forResource: fileName, withExtension: "json") else {
                return nil
            }
            
            let data = try Data(contentsOf: file)
         
            
            return data
        } catch {
            return nil
        }
    }
}
