//
//  UiStorboard+Extension.swift
//  Assignment
//
//  Created by Jay  on 2020-09-29.
//

import UIKit


extension UIStoryboard {
    static let main = "Main"
    
    private struct Identifier {
        static let newsDetailsVC = "NewsDetailsVC"
    }
    
    
    
    // saleTableVC view controller
    class func instantiateNewsDetailsVC(vm: NewsDetailsViewModel) -> NewsDetailsVC {
        
        guard let viewController = mainStoryboad().instantiateViewController(withIdentifier: Identifier.newsDetailsVC)
            as? NewsDetailsVC else {
                fatalError("Vc with Identifider \(Identifier.newsDetailsVC) not found")
        }
        viewController.vm = vm
        return viewController
    }

    private class func mainStoryboad() -> UIStoryboard {
        return UIStoryboard(name: main, bundle: nil)
    }
}
