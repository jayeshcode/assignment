//
//  Dictionary+Extension.swift
//  Assignment
//
//  Created by Jay  on 2020-09-29.
//

import Foundation

extension Dictionary where Key:ExpressibleByStringLiteral, Value:ExpressibleByStringLiteral  {
    func debugPrint() {
        print("{")
        for (key, value) in self {
            print("    \(key) -> \(value)")
        }
        print("}")
    }
}


extension Result {
  init(success: Success?, error: Failure?, defaultError: Failure) {
    if let error = error {
      self = .failure(error)
    } else if let success = success {
      self = .success(success)
    } else {
      self = .failure(defaultError)
    }
  }

  var error: Failure? {
    if case let .failure(error) = self {
      return error
    } else {
      return nil
    }
  }
}
