//
//  Data+Extension.swift
//  Assignment
//
//  Created by Jay  on 2020-09-29.
//

import Foundation


extension Data {
    func debugPrint() {
        do {
            try self.debugJsonPrint()
        } catch {
            self.debugStringPrint()
        }
    }
    
    func debugJsonPrint() throws {
        let errorObject = try JSONSerialization.jsonObject(with: self, options: JSONSerialization.ReadingOptions.mutableContainers)
        print(errorObject)
    }
    
    func debugStringPrint() {
        if let responseString = String(data: self, encoding: .utf8) {
            print(responseString)
        } else {
            print(self.debugDescription)
        }
    }
}

extension URL {
    var isHttps: Bool {
        return (scheme ?? "") == "https" 
    }
}
