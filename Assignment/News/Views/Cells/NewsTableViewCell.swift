//
//  NewsTableViewCell.swift
//  Assignment
//
//  Created by Jay  on 2020-09-29.
//

import UIKit

class NewsTableViewCell: UITableViewCell {
    
    // MARK: Outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var newsImageView: UIImageView!
    @IBOutlet weak var imageViewHeightConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    /// Constants used in NewsTableViewCell
    private struct Constants {
        static let intialImageHeight: CGFloat = 100.0
        static let cellPadding: CGFloat = 80.0 // L 40 R 40
    } // End of constants
    
    // MARK: Methods
    
    /// Configure Cell
    /// - Parameter vm: NewsCellModel
    func configure(vm: NewsCellModel) {
        
        // Show title
        titleLabel.text = vm.title
        
        // Show Image
        if vm.type == .imageNewsCell {
            
            imageViewHeightConstraint.constant = Constants.intialImageHeight
            let ImageViewWidth = frame.width - Constants.cellPadding
            
            image(smallThumbnail: vm.smallThumbnail) { image in
                self.newsImageView.image = image
            }
            
            let childData = vm.childData
            
            if let imgHeight = childData?.thumbnailHeight,
               let imgWidth = childData?.thumbnailWidth {
                
                let aspectRatio = imgWidth/imgHeight
                let calculatedHeight = ImageViewWidth / CGFloat(aspectRatio)
                imageViewHeightConstraint.constant = calculatedHeight
                
            }
            

        }
        
        // showDisclosure
        if !vm.hideDisclosure {
            accessoryType = .disclosureIndicator
        }
    } // End of configure
    
    /// Fetch image from cache
    /// - Parameters:
    ///   - smallThumbnail: String smallThumbnail
    ///   - completion: Returns image or nil
    private func image(smallThumbnail: String, completion: @escaping imageClosure)  {
        ExpiringCache.newsImages(smallThumbnail: smallThumbnail, completion: { image in
            DispatchQueue.main.async {
                completion(image)
            }
        })
    } // End of image
}
