//
//  NewsCellModel.swift
//  Assignment
//
//  Created by Jay  on 2020-09-30.
//

import Foundation

enum NewsCellType: String {
    case newsCell = "NewsTableViewCell"
    case imageNewsCell = "NewsImageTableViewCell"
    case staticCell
}

class NewsCellModel {
    
    var type: NewsCellType = .newsCell
    var title = ""
    var smallThumbnail = ""
    var hideDisclosure: Bool
    var childData: ChildData?
    
    
    init(cellType: NewsCellType = .newsCell, title: String = "", smallThumbnail:String = "" , hideDisclosure: Bool = false, showBottomBar: Bool = false, childData: ChildData? = nil) {
        
        self.title = title
        self.smallThumbnail = smallThumbnail
        self.type = cellType
        self.hideDisclosure = hideDisclosure
        self.childData = childData
    }
}

