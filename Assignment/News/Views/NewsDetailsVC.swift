//
//  NewsDetailsVC.swift
//  Assignment
//
//  Created by Jay  on 2020-09-30.
//

import UIKit

class NewsDetailsVC: UIViewController {
    
    // MARK: Outlets
    @IBOutlet weak var ImageDetail: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    // Force unwrapping as vm intialis is compulsory for details page
    var vm: NewsDetailsViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "News Detail"
        vm.refreshUI = {[weak self] in self?.refreshUI()}
        vm.fetchImage()
        
    }
    
    /// refreshUI
    private func refreshUI() {
        titleLabel.text =   vm.detailsTitle
        ImageDetail.image = vm.detailImage
    }
}
