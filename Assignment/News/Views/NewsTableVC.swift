//
//  NewsTableVC.swift
//  Assignment
//
//  Created by Jay  on 2020-09-29.
//

import UIKit

class NewsTableVC: UIViewController {
    
    // MARK: Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    // MARK: Variables
    let vm = NewsViewModel()
    let spinner = UIActivityIndicatorView(style: .medium)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupClosures()
        vm.fetchSwiftNews(input: "java")
        
       
    } // End of viewDidLoad
    
    // MARK: Helper Methods
    
    /// Method to setup UI
    private func setupUI() {
        title = "Swift News"
        tableView.tableFooterView = UIView(frame: .zero)
        navigationController?.navigationBar.tintColor = vm.loblawsColor
        
        spinner.hidesWhenStopped = true
        spinner.color = vm.loblawsColor
        tableView.backgroundView = spinner
    } // End of setupUI
    
    private func setupClosures() {
        vm.refreshUI = {[weak self] in self?.refreshUI()}
        vm.showSpinner = { [weak self] in self?.showSpinner() }
        vm.hideSpinner = { [weak self] in self?.hideSpinner() }
        vm.showError = {[weak self] (error) in
            self?.showError(error: error)
        } // End of setupClosures
    }
    
    
    private func refreshUI() {
        tableView.reloadData()
    }
    
    private func showSpinner() {
        spinner.startAnimating()
    }
    
    private func hideSpinner() {
        spinner.stopAnimating()
    }
    
//    @objc func editingDidEnd() {
//        
//        guard let !text.isempty else {
//            return
//        }
//
//        vm.fetchSwiftNews(input: text)
//
//    }
    
}

// MARK: Tableview Datasource & delegate
extension NewsTableVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  vm.displayList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cellVM = vm.displayList[indexPath.row]
        let identifier  = cellVM.type == .staticCell ? "NewsTableViewCell" : cellVM.type.rawValue
        guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? NewsTableViewCell else {
            return UITableViewCell()
        }
        
       
        if let selectedIndex = vm.selectedIndex {
            cell.backgroundColor = selectedIndex == indexPath  ? .blue : .white
        }
        
        cell.configure(vm: cellVM)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cellVM = vm.displayList[indexPath.row]
        
    
        let newsDetailsVM = NewsDetailsViewModel(smallThumbnail: cellVM.smallThumbnail, title: cellVM.title, cellType: cellVM.type)
        
        vm.selectedIndex = indexPath
        
        if cellVM.type == .staticCell {
            let selected = vm.selections[indexPath.row]
            //Make a call
            vm.fetchSwiftNews(input: selected)
            return
        }
      NavigationRouter.showNewsNewsDetailsVC(vm: newsDetailsVM)
    }
    
    
}

// MARK: BaseProtocol
extension NewsTableVC: BaseProtocol {
    fileprivate  func showError(error: Error)  {
        
        var message = "Unable to get data from server."
        #if DEBUG
        message = error.localizedDescription
        #endif
        
        self.showAlertWithText(message: message)
    }
}
