//
//  SwiftNews.swift
//  Assignment
//
//  Created by Jay  on 2020-09-29.
//


import Foundation

// MARK: - SwiftNews
struct SwiftNews: Codable {
    let kind: String
    let data: SwiftNewsData
}

// MARK: - SwiftNewsData
struct SwiftNewsData: Codable {
    let modhash: String
    let dist: Int
    let children: [Child]
    let after: String
    let before: String?
}

// MARK: - Child
struct Child: Codable {
    let kind: String?
    let data: ChildData
}

// MARK: - ChildData
struct ChildData: Codable {
    let approvedAtUTC: String?
    let subreddit: String?
    let selftext, authorFullname: String
    let saved: Bool
    let modReasonTitle: String?
    let gilded: Int
    let clicked: Bool
    let title: String
    let subredditNamePrefixed: String?
    let hidden: Bool
    let linkFlairCSSClass: String?
    let downs: Int
    let thumbnailHeight: Double?
    let topAwardedType: String?
    let hideScore: Bool
    let name: String
    let quarantine: Bool
    let linkFlairTextColor: FlairTextColor
    let upvoteRatio: Double
    let authorFlairBackgroundColor: String?
    let subredditType: SubredditType
    let ups, totalAwardsReceived: Int
    let mediaEmbed: MediaEmbed
    let thumbnailWidth: Double?
    let authorFlairTemplateID: String?
    let isOriginalContent: Bool
    let userReports: [String]?
    let isRedditMediaDomain, isMeta: Bool
    let category: String?
    let secureMediaEmbed: MediaEmbed?
    let linkFlairText: String?
    let canModPost: Bool
    let score: Int
    let approvedBy: String?
    let authorPremium: Bool
    let thumbnail: String
    let edited: Edited
    let authorFlairCSSClass: String?
    let authorFlairRichtext: [String]?
    let gildings: Gildings
    let contentCategories: String?
    let isSelf: Bool
    let modNote: String?
    let created: Int
    let linkFlairType: String?
    let removedByCategory, bannedBy: String?
    let authorFlairType: String?
    let domain: String
    let allowLiveComments: Bool
    let selftextHTML: String?
    let likes, suggestedSort, bannedAtUTC, viewCount: String?
    let archived, noFollow, isCrosspostable, pinned: Bool
    let over18: Bool
   // let allAwardings, awarders: [String]?
    let mediaOnly, canGild, spoiler, locked: Bool
    let authorFlairText: String?
    let treatmentTags: [String]?
    let visited: Bool
    let removedBy, numReports, distinguished: String?
    let subredditID: String?
    let modReasonBy, removalReason: String?
    let linkFlairBackgroundColor: String?
    let id: String
    let isRobotIndexable: Bool
    let reportReasons: String?
    let author: String
    let discussionType: String?
    let numComments: Int
    let sendReplies: Bool
    let whitelistStatus: String?
    let contestMode: Bool
    let modReports: [String]?
    let authorPatreonFlair: Bool
    let authorFlairTextColor: FlairTextColor?
    let permalink: String
    let parentWhitelistStatus: String?
    let stickied: Bool
    let url: String
    let subredditSubscribers, createdUTC, numCrossposts: Int
    //let media: Media?
    let isVideo: Bool
    let preview: Preview?
    //let crosspostParentList: [CrosspostParentList]?
    let crosspostParent, linkFlairTemplateID: String?

    enum CodingKeys: String, CodingKey {
        case approvedAtUTC = "approved_at_utc"
        case subreddit, selftext
        case authorFullname = "author_fullname"
        case saved
        case modReasonTitle = "mod_reason_title"
        case gilded, clicked, title
        case subredditNamePrefixed = "subreddit_name_prefixed"
        case hidden
        case linkFlairCSSClass = "link_flair_css_class"
        case downs
        case thumbnailHeight = "thumbnail_height"
        case topAwardedType = "top_awarded_type"
        case hideScore = "hide_score"
        case name, quarantine
        case linkFlairTextColor = "link_flair_text_color"
        case upvoteRatio = "upvote_ratio"
        case authorFlairBackgroundColor = "author_flair_background_color"
        case subredditType = "subreddit_type"
        case ups
        case totalAwardsReceived = "total_awards_received"
        case mediaEmbed = "media_embed"
        case thumbnailWidth = "thumbnail_width"
        case authorFlairTemplateID = "author_flair_template_id"
        case isOriginalContent = "is_original_content"
        case userReports = "user_reports"
        case isRedditMediaDomain = "is_reddit_media_domain"
        case isMeta = "is_meta"
        case category
        case secureMediaEmbed = "secure_media_embed"
        case linkFlairText = "link_flair_text"
        case canModPost = "can_mod_post"
        case score
        case approvedBy = "approved_by"
        case authorPremium = "author_premium"
        case thumbnail, edited
        case authorFlairCSSClass = "author_flair_css_class"
        case authorFlairRichtext = "author_flair_richtext"
        case gildings
        case contentCategories = "content_categories"
        case isSelf = "is_self"
        case modNote = "mod_note"
        case created
        case linkFlairType = "link_flair_type"
        case removedByCategory = "removed_by_category"
        case bannedBy = "banned_by"
        case authorFlairType = "author_flair_type"
        case domain
        case allowLiveComments = "allow_live_comments"
        case selftextHTML = "selftext_html"
        case likes
        case suggestedSort = "suggested_sort"
        case bannedAtUTC = "banned_at_utc"
        case viewCount = "view_count"
        case archived
        case noFollow = "no_follow"
        case isCrosspostable = "is_crosspostable"
        case pinned
        case over18 = "over_18"
        //case allAwardings = "all_awardings"
        //case awarders
        case mediaOnly = "media_only"
        case canGild = "can_gild"
        case spoiler, locked
        case authorFlairText = "author_flair_text"
        case treatmentTags = "treatment_tags"
        case visited
        case removedBy = "removed_by"
        case numReports = "num_reports"
        case distinguished
        case subredditID = "subreddit_id"
        case modReasonBy = "mod_reason_by"
        case removalReason = "removal_reason"
        case linkFlairBackgroundColor = "link_flair_background_color"
        case id
        case isRobotIndexable = "is_robot_indexable"
        case reportReasons = "report_reasons"
        case author
        case discussionType = "discussion_type"
        case numComments = "num_comments"
        case sendReplies = "send_replies"
        case whitelistStatus = "whitelist_status"
        case contestMode = "contest_mode"
        case modReports = "mod_reports"
        case authorPatreonFlair = "author_patreon_flair"
        case authorFlairTextColor = "author_flair_text_color"
        case permalink
        case parentWhitelistStatus = "parent_whitelist_status"
        case stickied, url
        case subredditSubscribers = "subreddit_subscribers"
        case createdUTC = "created_utc"
        case numCrossposts = "num_crossposts"
        case isVideo = "is_video"
        case preview
        //case crosspostParentList = "crosspost_parent_list"
        case crosspostParent = "crosspost_parent"
        case linkFlairTemplateID = "link_flair_template_id"
    }
}

enum FlairTextColor: String, Codable {
    case dark = "dark"
    case light = "light"
}


// MARK: - CrosspostParentList
//struct CrosspostParentList: Codable {
//    let approvedAtUTC: String?
//    let subreddit, selftext, authorFullname: String
//    let saved: Bool
//    let modReasonTitle: String?
//    let gilded: Int
//    let clicked: Bool
//    let title: String
//    let subredditNamePrefixed: String
//    let hidden: Bool
//    let  linkFlairCSSClass: String?
//    let downs, thumbnailHeight: Double?
//    let topAwardedType: String?
//    let hideScore: Bool
//    let name: String
//    let quarantine: Bool
//    let linkFlairTextColor: String?
//    let upvoteRatio: Double
//    let authorFlairBackgroundColor: String?
//    let subredditType: SubredditType
//    let ups, totalAwardsReceived: Int
//    let mediaEmbed: Gildings
//    let thumbnailWidth: Double?
//    let authorFlairTemplateID: String?
//    let isOriginalContent: Bool
//    let userReports: [String]?
//    let isRedditMediaDomain, isMeta: Bool
//    let category: String?
//    let secureMediaEmbed: Gildings
//    let linkFlairText: String?
//    let canModPost: Bool
//    let score: Int
//    let approvedBy: String?
//    let authorPremium: Bool
//    let thumbnail: String
//    let edited: Bool
//    let authorFlairCSSClass: String?
//    let authorFlairRichtext: [String]?
//    let gildings: Gildings
//    let contentCategories: String?
//    let isSelf: Bool
//    let modNote: String?
//    let created: Int
//    let linkFlairType: String?
//    let removedByCategory, bannedBy: String?
//    let authorFlairType: String?
//    let domain: String
//    let allowLiveComments: Bool
//    let selftextHTML, likes, suggestedSort, bannedAtUTC: String?
//    let viewCount: String?
//    let archived, noFollow, isCrosspostable, pinned: Bool
//    let over18: Bool
//    let preview: Preview?
//    let allAwardings, awarders: [String]?
//    let mediaOnly, canGild, spoiler, locked: Bool
//    let authorFlairText: String?
//    let treatmentTags: [String]?
//    let visited: Bool
//    let removedBy, numReports, distinguished: String?
//    let subredditID: String
//    let modReasonBy, removalReason: String?
//    let linkFlairBackgroundColor, id: String
//    let isRobotIndexable: Bool
//    let reportReasons: String?
//    let author: String
//    let discussionType: String?
//    let numComments: Int
//    let sendReplies: Bool
//    let whitelistStatus: String?
//    let contestMode: Bool
//    let modReports: [String]?
//    let authorPatreonFlair: Bool
//    let authorFlairTextColor: String?
//    let permalink: String
//    let parentWhitelistStatus: String?
//    let stickied: Bool
//    let url: String
//    let subredditSubscribers, createdUTC, numCrossposts: Int
//    let media: String?
//    let isVideo: Bool
//
//    enum CodingKeys: String, CodingKey {
//        case approvedAtUTC = "approved_at_utc"
//        case subreddit, selftext
//        case authorFullname = "author_fullname"
//        case saved
//        case modReasonTitle = "mod_reason_title"
//        case gilded, clicked, title
//        case subredditNamePrefixed = "subreddit_name_prefixed"
//        case hidden
//        case linkFlairCSSClass = "link_flair_css_class"
//        case downs
//        case thumbnailHeight = "thumbnail_height"
//        case topAwardedType = "top_awarded_type"
//        case hideScore = "hide_score"
//        case name, quarantine
//        case linkFlairTextColor = "link_flair_text_color"
//        case upvoteRatio = "upvote_ratio"
//        case authorFlairBackgroundColor = "author_flair_background_color"
//        case subredditType = "subreddit_type"
//        case ups
//        case totalAwardsReceived = "total_awards_received"
//        case mediaEmbed = "media_embed"
//        case thumbnailWidth = "thumbnail_width"
//        case authorFlairTemplateID = "author_flair_template_id"
//        case isOriginalContent = "is_original_content"
//        case userReports = "user_reports"
//        case isRedditMediaDomain = "is_reddit_media_domain"
//        case isMeta = "is_meta"
//        case category
//        case secureMediaEmbed = "secure_media_embed"
//        case linkFlairText = "link_flair_text"
//        case canModPost = "can_mod_post"
//        case score
//        case approvedBy = "approved_by"
//        case authorPremium = "author_premium"
//        case thumbnail, edited
//        case authorFlairCSSClass = "author_flair_css_class"
//        case authorFlairRichtext = "author_flair_richtext"
//        case gildings
//        case contentCategories = "content_categories"
//        case isSelf = "is_self"
//        case modNote = "mod_note"
//        case created
//        case linkFlairType = "link_flair_type"
//        case removedByCategory = "removed_by_category"
//        case bannedBy = "banned_by"
//        case authorFlairType = "author_flair_type"
//        case domain
//        case allowLiveComments = "allow_live_comments"
//        case selftextHTML = "selftext_html"
//        case likes
//        case suggestedSort = "suggested_sort"
//        case bannedAtUTC = "banned_at_utc"
//        case viewCount = "view_count"
//        case archived
//        case noFollow = "no_follow"
//        case isCrosspostable = "is_crosspostable"
//        case pinned
//        case over18 = "over_18"
//        case preview
//        case allAwardings = "all_awardings"
//        case awarders
//        case mediaOnly = "media_only"
//        case canGild = "can_gild"
//        case spoiler, locked
//        case authorFlairText = "author_flair_text"
//        case treatmentTags = "treatment_tags"
//        case visited
//        case removedBy = "removed_by"
//        case numReports = "num_reports"
//        case distinguished
//        case subredditID = "subreddit_id"
//        case modReasonBy = "mod_reason_by"
//        case removalReason = "removal_reason"
//        case linkFlairBackgroundColor = "link_flair_background_color"
//        case id
//        case isRobotIndexable = "is_robot_indexable"
//        case reportReasons = "report_reasons"
//        case author
//        case discussionType = "discussion_type"
//        case numComments = "num_comments"
//        case sendReplies = "send_replies"
//        case whitelistStatus = "whitelist_status"
//        case contestMode = "contest_mode"
//        case modReports = "mod_reports"
//        case authorPatreonFlair = "author_patreon_flair"
//        case authorFlairTextColor = "author_flair_text_color"
//        case permalink
//        case parentWhitelistStatus = "parent_whitelist_status"
//        case stickied, url
//        case subredditSubscribers = "subreddit_subscribers"
//        case createdUTC = "created_utc"
//        case numCrossposts = "num_crossposts"
//        case media
//        case isVideo = "is_video"
//    }
//}

// MARK: - Gildings
struct Gildings: Codable {
}

// MARK: - Preview
struct Preview: Codable {
    let images: [Image]
    let enabled: Bool
}

// MARK: - Image
struct Image: Codable {
    let source: Source
    let resolutions: [Source]
    let variants: Gildings
    let id: String
}

// MARK: - Source
struct Source: Codable {
    let url: String
    let width, height: Int
}

enum SubredditType: String, Codable {
    case subredditTypePublic = "public"
}

enum Edited: Codable {
    case bool(Bool)
    case integer(Int)

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(Bool.self) {
            self = .bool(x)
            return
        }
        if let x = try? container.decode(Int.self) {
            self = .integer(x)
            return
        }
        throw DecodingError.typeMismatch(Edited.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for Edited"))
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .bool(let x):
            try container.encode(x)
        case .integer(let x):
            try container.encode(x)
        }
    }
}



// MARK: - Media
struct Media: Codable {
    let type: String
    let oembed: Oembed
}

// MARK: - Oembed
struct Oembed: Codable {
    let providerURL: String
    let version, title, type: String
    let thumbnailWidth, height, width: Double?
    let html, authorName, providerName: String
    let thumbnailURL: String
    let thumbnailHeight: Double?
    let authorURL: String

    enum CodingKeys: String, CodingKey {
        case providerURL = "provider_url"
        case version, title, type
        case thumbnailWidth = "thumbnail_width"
        case height, width, html
        case authorName = "author_name"
        case providerName = "provider_name"
        case thumbnailURL = "thumbnail_url"
        case thumbnailHeight = "thumbnail_height"
        case authorURL = "author_url"
    }
}

// MARK: - MediaEmbed
struct MediaEmbed: Codable {
    let content: String?
    let width: Int?
    let scrolling: Bool?
    let height: Int?
    let mediaDomainURL: String?

    enum CodingKeys: String, CodingKey {
        case content, width, scrolling, height
        case mediaDomainURL = "media_domain_url"
    }
}






