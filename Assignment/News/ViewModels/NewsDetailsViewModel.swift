//
//  NewsDetailsViewModel.swift
//  Assignment
//
//  Created by Jay  on 2020-09-30.
//

import UIKit


class  NewsDetailsViewModel {
    
    // MARK: properties
    var smallThumbnail = ""
    var title = ""
    var cellType = NewsCellType.newsCell
    
    var refreshUI: emptyClosure?
    var detailImage: UIImage? {
        didSet {
            refreshUI?()
        }
    }
    
    var detailsTitle: String {
        return cellType == .newsCell ? title : ""
    }
    
    
    init(smallThumbnail: String, title: String, cellType: NewsCellType) {
        self.smallThumbnail = smallThumbnail
        self.title = title
        self.cellType = cellType
    }
    
    func fetchImage()  {
        ExpiringCache.newsImages(smallThumbnail: smallThumbnail, completion: {[weak self] image in
            DispatchQueue.main.async {
                self?.detailImage = image
            }
        })
    }
}
