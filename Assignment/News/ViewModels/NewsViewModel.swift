//
//  NewsViewModel.swift
//  Assignment
//
//  Created by Jay  on 2020-09-29.
//

import Foundation
import UIKit

typealias emptyClosure = (()->())
typealias ErrorClosure = ((Error) -> ())

class  NewsViewModel {
    // MARK: Properties
    
    var refreshUI: emptyClosure?
    var showSpinner: emptyClosure?
    var hideSpinner: emptyClosure?
    var showError: ErrorClosure?
    var displayList = [NewsCellModel]()
    
    var selectedIndex: IndexPath?
    
    let selections = ["java", "swift", "pics", "cars"]
   
    var swiftNews: SwiftNews? {
        didSet {
            createDisplayList()
            refreshUI?()
        }
    }
    
    var childrens : [Child] {
        guard let childrens = swiftNews?.data.children else {
            return  [Child]()
        }
        
        return childrens
    }
    
    var loblawsColor: UIColor {
        return #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1)
    }
    
    // MARK: Services
    func fetchSwiftNews(input: String) {
        showSpinner?()
        NewsService.fetchSwiftNews(input: input) { [weak self] result in
            self?.hideSpinner?()
            switch result {
            case .success(let SwiftNews):
                self?.swiftNews = SwiftNews
            case .failure(let error):
                print(error)
                self?.showError?(error)
            }
        }
    }
    
    // MARK: helper methods
    func createDisplayList () {
        displayList.removeAll()
       
        selections.forEach {
           
            displayList.append(NewsCellModel(cellType: .staticCell, title: $0, smallThumbnail: "", hideDisclosure: true, showBottomBar: false, childData: nil))
        }
       
       
        childrens.forEach {
            let childData = $0.data
            
            let cellType =  isThumbnailPresent(childData: childData) ? NewsCellType.imageNewsCell : NewsCellType.newsCell
            let title = childData.title
            let smallThumbnail = isThumbnailPresent(childData: childData) ? childData.thumbnail: ""
            
            
            let cellvm = NewsCellModel(cellType: cellType, title: title, smallThumbnail: smallThumbnail, hideDisclosure: false, showBottomBar: false, childData: childData)
            
            displayList.append(cellvm)
        }
    } // End of createDisplayList
    
    
    /// isThumbnailPresent
    /// - Parameter childData: isThumbnailPresent
    /// - Returns: Thumbnail is present
    func isThumbnailPresent(childData: ChildData) -> Bool {
        
        guard  let _ = childData.thumbnailHeight,
               let _ = childData.thumbnailWidth else {
            return false
        }
        
        return !childData.thumbnail.isEmpty 
    } //End of isThumbnailPresent
    
}
