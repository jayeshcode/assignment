//
//  AppNavigation.swift
//  Assignment
//
//  Created by Jay  on 2020-09-29.
//

import UIKit


struct NavigationRouter {
    
    
    static func rootController() -> UINavigationController {
        
             guard  let first = UIApplication.shared.windows.first,
                    let navigationController =  first.rootViewController as? UINavigationController else {
                fatalError("navigationController does not exist")
        }
        
        return navigationController
    }
    
    // Show News Table VC
    static func showNewsNewsDetailsVC(vm: NewsDetailsViewModel) {
        rootController().pushViewController(UIStoryboard.instantiateNewsDetailsVC(vm: vm), animated: true)
}
}
