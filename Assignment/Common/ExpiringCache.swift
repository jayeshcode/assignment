//
//  ExpiringCache.swift
//  Assignment
//
//  Created by Jay  on 2020-09-30.
//

import UIKit

typealias imageClosure = ((UIImage?) -> Void)

class ExpiringCache {
    
    static var cache: NSCache<NSString, UIImage> = NSCache()
       let sharedInstance = ExpiringCache()
    
    
    
    init() {
        
    }
    
    static func newsImages(smallThumbnail: String, completion: @escaping imageClosure) {
        
        if let image = self.cache.object(forKey: smallThumbnail as NSString) {
            DispatchQueue.main.async {
            completion(image)
            }
        } else {

            var image: UIImage?
            if  let url = URL(string: smallThumbnail) {
                DispatchQueue.global(qos: .background).async {
                    if let data = try? Data(contentsOf: url),
                        let img = UIImage(data: data) {
                        image = img
                        self.cache.setObject(image!, forKey: smallThumbnail as NSString)
                        DispatchQueue.main.async {
                        completion(image)
                        }
                    }
                }
            }
              DispatchQueue.main.async {
            completion(image)
            }
        }
        
    }
}
