//
//  RequestFactory.swift
//  Assignment
//
//  Created by Jay  on 2020-09-29.
//

import Foundation


public struct  RequestFactory {
    var  urlRequest: URLRequest
    var urlType : HTTPMethod = .get
    var urlString = ""
    var url: URL?
    
    
    init?(urlString: String, urlType : HTTPMethod = .get) {
        
        guard let url = URL(string: urlString) else {
            return nil
        }
        
        self.url = url
        self.urlType = urlType
        self.urlRequest =  GenericService.getURLRequest(for: url, method: urlType)
    }
    
}


enum RequestError: Error {
    case invalidURL
    case parseError
    case requestError
    case invalidPath
}

public struct EmptyResultError: Error {}
