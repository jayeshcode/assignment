//
//  NewsService.swift
//  Assignment
//
//  Created by Jay  on 2020-09-29.
//

import Foundation

class NewsService {
    public static func fetchSwiftNews(input:String, completion:@escaping (Result<SwiftNews, Error>) -> Void) {
        
        let swiftNewsUrl = "https://www.reddit.com/r/\(input).json"
        
        guard let request = RequestFactory(urlString: swiftNewsUrl) else {
            completion(.failure(RequestError.invalidURL))
            return
        }
        
        GenericService.fetch(with: request, decodableType: SwiftNews.self) { result in
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
}
