//
//  GenericService.swift
//  Assignment
//
//  Created by Jay  on 2020-09-29.
//

import Foundation

class GenericService {
    
    static func fetch<T:Decodable>(with request: RequestFactory, decodableType: T.Type, completion: @escaping (Result<T ,Error>) -> Void) {
        
        decodingTask(with: request) {
            let result = Result(success: $0, error: $2, defaultError: EmptyResultError()).flatMap {
                data in
                Result(catching: {
                    try JSONDecoder().decode(decodableType.self, from: data)
                })
            }
            completion(result)
        }
    }
    
    static func decodingTask(with request: RequestFactory, completion: @escaping (Data?, URLResponse?, Error?)->Void) {
        
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 300
        configuration.waitsForConnectivity = true
        configuration.requestCachePolicy = .reloadIgnoringLocalCacheData
        configuration.urlCache = nil
        
        let session = URLSession(configuration: configuration)
        
        let task = session.dataTask(with: request.urlRequest) { (data, response, error) in
            completion(data, response, error)
            if let data = data,
               let httpResponse = response as? HTTPURLResponse {
                debugPrint(httpResponse, data, request.urlRequest)
            }
            
        }
        task.resume()
    }
    
    fileprivate static func debugPrint(_ httpResponse: HTTPURLResponse, _ data: Data, _ request : URLRequest) {
        
        #if DEBUG
        print("\n============ RESPONSE - Start ============")
        print("ServiceType: \(String(describing: request.url))")
        print("Status Code: \(httpResponse.statusCode )")
        print("Response Headers:")
        if let allHeaders = request.allHTTPHeaderFields {
            allHeaders.debugPrint()
        } else {
            print(httpResponse.allHeaderFields.debugDescription)
        }
        print(" Response Body: ")
        data.debugPrint()
        print("============ RESPONSE - End ==============\n")
        #endif
    }
    
    
    static func baseAuthHeaders(authHeaders: String) -> [String: String] {
        let passwordString = authHeaders
        let passwordData = passwordString.data(using: String.Encoding.utf8)
        guard let base64LoginData = passwordData?.base64EncodedString(options: Data.Base64EncodingOptions.lineLength76Characters) else {
            assertionFailure("could not convert into base64 string")
            return [:]
        }
        
        let headers = ["authorization": "Basic \(base64LoginData)"]
        return headers
    }
    
    
    static func getURLRequest(for url: URL, method: HTTPMethod, authHeaders: String = "" ) -> URLRequest {
        var request = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 0)
        request.httpMethod = method.value()
        if !authHeaders.isEmpty {
            request.allHTTPHeaderFields = baseAuthHeaders(authHeaders: authHeaders)
        }
        return request
    }
}


public enum HTTPMethod: String {
    case get
    case post
    
    func value() -> String {
        return self.rawValue.uppercased()
    }
}





